import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, TextInput } from 'react-native';
import { COLORS } from '../../helpers/constants';
import { moderateScale, verticalScale } from '../../helpers/ScailingScreen';


function Input(props) {

    onfocus = () => {

    }

    return (
        <TextInput
            style={styles.input}
            placeholder={props.placeholder}
            placeholderTextColor={COLORS.darkGrey}
            
        />
    )
}

export default Input;

const styles = StyleSheet.create({
    input: {
        height: verticalScale(45),
        width: "100%",
        backgroundColor: "white",
        justifyContent: "center",
        alignItems: "center",
        marginVertical: verticalScale(5),
        borderBottomWidth: 2,
        borderColor: COLORS.darkGrey
    },
    buttonText: {
        fontWeight: "600",
        fontSize: moderateScale(14)
    }
})