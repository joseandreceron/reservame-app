import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';

import { moderateScale, verticalScale } from '../../helpers/ScailingScreen';
import DatePicker from 'react-native-datepicker';
import { COLORS } from '../../helpers/constants';


function CustomDatePicker(props) {
    return (
        <DatePicker
            date={props.value}
            mode={props.mode}
            placeholder={props.placeholder}
            is24Hour={false}
            confirmBtnText="Confirm"
            cancelBtnText="Cancel"
            showIcon={false}
            customStyles={{
                dateInput: styles.dateInput,
                placeholderText: styles.datePlaceholderText,
                dateText: styles.dateText,
                dateTouchBody: styles.dateTouchBody,
                btnTextConfirm: styles.btnTextConfirm,
                datePicker: styles.datePickerSelect,
            }}
            onDateChange={props.action}
        />
    )
}

export default CustomDatePicker;

const styles = StyleSheet.create({
    datePicker: {
        width: '100%',
        marginBottom: 20,
        paddingBottom: 10,
        paddingLeft: 20,
        paddingRight: 20,
        height: 52,
        backgroundColor: "red",
        borderColor: '#E6E6F0',
        borderRadius: 50,
        borderWidth: 1,
    },
    datePickerSelect: {
        backgroundColor: '#d0d4db',
    },
    datePickerCon: {
        backgroundColor: '#EFF1F2',
    },
    dateInput: {
        margin: 0,
        borderWidth: 0,
        alignItems: 'flex-start',
    },
    datePlaceholderText: {
        fontSize: 16,
        color: COLORS.darkGrey,
    },
    dateText: {
        fontSize: 16,
        color: COLORS.darkgrey,
    },
    dateBtnTextConfirm: {
        color: COLORS.darkgrey,
    },
    placeholder: {
        textTransform: 'uppercase'
    },
    datepicker: {
        minWidth: 140,
        marginTop: -10,
        height: 45,
    },
    dateTouchBody: {
        height: 45,
    },
})