import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { moderateScale, verticalScale } from '../../helpers/ScailingScreen';


function Button(props) {
    return (
        <TouchableOpacity
            style={[styles.button, { backgroundColor: props.backgroundColor }]}
            onPress={props.onPress ? () => props.onPress() : console.log("OnPress")}
        >
            <Text style={[styles.buttonText, { color: props.titleColor }]}>{props.title}</Text>
        </TouchableOpacity>
    )
}

export default Button;

const styles = StyleSheet.create({
    button: {
        height: verticalScale(35),
        width: moderateScale(280),
        backgroundColor: "red",
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 25,
        shadowColor: 'black',
        shadowOffset: { width: 3, height: 3 },
        shadowOpacity: 0.2,
        elevation: 2,
        marginVertical: verticalScale(5)
    },
    buttonText: {
        fontWeight: "600",
        fontSize: moderateScale(14)
    }
})