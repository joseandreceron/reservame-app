import React from 'react';
import { View, Image } from 'react-native';
import { COLORS } from '../../helpers/constants';
import { SvgSelector } from '../../helpers/SvgSelector';


function TabNavIcon(props) {
    return (
        <SvgSelector
            icons={props.icon}
            size={25}
            color={props.focused ? COLORS.orange : COLORS.darkGrey}
        />
    )
}

export default TabNavIcon;