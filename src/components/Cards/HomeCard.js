import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { COLORS } from '../../helpers/constants';
import { moderateScale, verticalScale } from '../../helpers/ScailingScreen';

import { SvgSelector } from '../../helpers/SvgSelector';

function HomeCard(props) {
    return (
        <TouchableOpacity
            style={styles.button}
            onPress={props.onPress ? () => props.onPress() : console.log("OnPress")}
        >
            <SvgSelector
                icons={props.icon}
                size={35}
            />
            <Text style={styles.buttonText}>{props.title}</Text>
        </TouchableOpacity>
    )
}

export default HomeCard;

const styles = StyleSheet.create({
    button: {
        height: verticalScale(90),
        width: moderateScale(160),
        backgroundColor: "white",
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 5,
        marginVertical: verticalScale(5),
        marginHorizontal: moderateScale(10),
        borderColor: COLORS.darkGrey,
        borderWidth: 1
    },
    buttonText: {
        fontSize: moderateScale(14),
        color: COLORS.darkGrey,
        marginTop: verticalScale(5)
    }
})