import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image } from 'react-native';
import { COLORS } from '../../helpers/constants';
import { moderateScale, scale, verticalScale } from '../../helpers/ScailingScreen';

import { SvgSelector } from '../../helpers/SvgSelector';

function RestaurantCards(props) {
    console.log(props)
    return (
        <TouchableOpacity
            style={styles.button}
            onPress={props.onPress ? () => props.onPress() : console.log("OnPress")}
        >

            <Image
                source={{ uri: props.image }}
                style={styles.image}
            />

        <View style={styles.wrapper}>
            <Text style={styles.title}>{props.title}</Text>
            <Text style={styles.subtitle}>{props.schedule}</Text>
            <Text style={styles.subtitle}>{props.address}</Text>
        </View>

        </TouchableOpacity>
    )
}

export default RestaurantCards;

const styles = StyleSheet.create({
    button: {
        height: verticalScale(100),
        width: moderateScale(350),
        borderRadius: 5,
        marginVertical: verticalScale(5),
        marginHorizontal: moderateScale(10),
        borderColor: COLORS.darkGrey,
        padding: scale(10),
        flexDirection: "row",
        borderBottomWidth: 1,
        alignItems: "center"
    },
    image: {
        height: 90,
        width: 130,
        borderRadius: scale(10)
    },
    wrapper: {
        marginLeft: moderateScale(20)
    },
    title: {
        fontSize: moderateScale(18),
        color: COLORS.darkGrey,
        marginTop: verticalScale(5),
        fontWeight: "bold",
        marginVertical: verticalScale(2),
    },
    subtitle: {
        fontSize: moderateScale(12),
        marginVertical: verticalScale(2),
        color: COLORS.darkGrey
    }
})