import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, TextInput } from 'react-native';

import { COLORS } from '../../helpers/constants';
import { moderateScale, verticalScale } from '../../helpers/ScailingScreen';

import Ionicons from "react-native-vector-icons/Ionicons";

function Header(props) {
    return (
        <View style={styles.container}>

            <View style={styles.wrapper}>
                <TouchableOpacity
                    style={styles.iconButtons}
                    onPress={props.hasLeftFunc ? () => props.hasLeftFunc() : console.log("LeftIcon Pressed")}
                >
                    {props.hasLeftIcon &&
                        <Ionicons
                            name={props.leftIcon ? props.leftIcon : 'ios-arrow-back'}
                            size={30}
                            color={COLORS.white}
                        />
                    }
                </TouchableOpacity>

                <Text style={styles.titlteText}>{props.title}</Text>

                <TouchableOpacity
                    style={styles.iconButtons}
                    onPress={props.hasRightFunc ? () => props.hasRightFunc() : console.log("RightIcon Pressed")}
                >
                    {props.hasRightIcon &&
                        <Ionicons
                            name={props.rightIcon ? props.rightIcon : 'ios-arrow-back'}
                            size={30}
                            color={COLORS.white}
                        />
                    }
                </TouchableOpacity>
            </View>

        </View>
    )
}

export default Header;

const styles = StyleSheet.create({
    container: {
        backgroundColor: COLORS.green,
        width: "100%",
        height: verticalScale(150),
        marginTop: verticalScale(-50),
        justifyContent: "center",
        alignItems: "center",
    },
    titlteText: {
        fontSize: moderateScale(24),
        color: COLORS.white,
        marginTop: verticalScale(30)
    },
    wrapper: {
        flexDirection: "row",
        width: "100%",
        justifyContent: "space-between",
        alignItems: "center",
        paddingHorizontal: moderateScale(10)
    },
    iconButtons: {
        marginTop: verticalScale(30),
        height: 50,
        width: 50,
        justifyContent: "center",
        alignItems: "center"
    }
})