import React, { Component } from 'react';
import { View, Text, StyleSheet, StatusBar, TouchableOpacity } from 'react-native';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'


import { scale, moderateScale, verticalScale, widthPercentageToDP } from '../../helpers/ScailingScreen';

export default function renderFixedForeground({ navigation, title, subtitle }) {
    return (
        <View style={styles.headerContainer} >
            <TouchableOpacity
                style={styles.headerBackButton}
                onPress={() => navigation.goBack()}
            >
                <FontAwesome5
                    name='arrow-left'
                    size={scale(20)}
                />
            </TouchableOpacity>

            <Text style={styles.imageTitle}>{title}</Text>
            <Text style={styles.subtitle}>{subtitle}</Text>
        </View>
    )
}
const styles = StyleSheet.create({
    headerContainer: {
        height: moderateScale(220),
        justifyContent: 'space-between',
        flexDirection: 'row',
        paddingHorizontal: moderateScale(20),
        paddingVertical: verticalScale(10),
        transform: [{ 'translate': [0, 0, 1] }],
    },
    imageTitle: {
        color: 'white',
        fontSize: moderateScale(24),
        fontWeight: 'bold',
        position: 'absolute',
        bottom: verticalScale(45),
        left: moderateScale(25),
        width: widthPercentageToDP('90%')
    },
    subtitle: {
        color: 'white',
        position: 'absolute',
        bottom: verticalScale(25),
        left: moderateScale(25),
    },
    headerBackButton: {
        height: 40,
        width: 40,
        backgroundColor: 'white',
        borderRadius: 25,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: Platform.OS === 'ios' ? verticalScale(35) : verticalScale(10)
    }
});