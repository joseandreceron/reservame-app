import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, TextInput } from 'react-native';
import { COLORS } from '../../helpers/constants';
import { heightPercentageToDP, moderateScale, verticalScale } from '../../helpers/ScailingScreen';

import Ionicons from "react-native-vector-icons/Ionicons"

function SearchBar(props) {
    return (
        <React.Fragment>
            {props.isButton ? (
                <TouchableOpacity
                    style={styles.searchContainer}
                    onPress={props.onPress ? () => props.onPress() : () => console.log("SearchBar")}
                >

                    {props.isBack &&
                        <React.Fragment>
                            <TouchableOpacity
                                onPress={props.onBackPress ? () => props.onBackPress() : () => console.log("Back Pressed")}
                            >
                                <Ionicons
                                    name="ios-arrow-back"
                                    size={30}
                                    color={COLORS.darkGrey}
                                />
                            </TouchableOpacity>

                            <View style={styles.divider} />
                        </React.Fragment>
                    }

                    <Ionicons
                        name="search"
                        color={COLORS.darkGrey}
                        size={moderateScale(30)}
                    />

                    <Text style={styles.search}>Buscar Servicios</Text>
                </TouchableOpacity>
            ) : (
                    <View style={styles.searchContainer}>

                        {props.isBack &&
                            <React.Fragment>
                                <TouchableOpacity
                                    onPress={props.onBackPress ? () => props.onBackPress() : () => console.log("Back Pressed")}
                                >
                                    <Ionicons
                                        name="ios-arrow-back"
                                        size={30}
                                        color={COLORS.darkGrey}
                                    />
                                </TouchableOpacity>

                                <View style={styles.divider} />
                            </React.Fragment>
                        }

                        <Ionicons
                            name="search"
                            color={COLORS.darkGrey}
                            size={moderateScale(30)}
                        />

                        <TextInput
                            style={styles.search}
                            placeholder={"Buscar Servicios"}
                            placeholderTextColor={COLORS.darkGrey}
                        />
                    </View>
                )
            }
        </React.Fragment >
    )
}

export default SearchBar;

const styles = StyleSheet.create({
    searchContainer: {
        width: moderateScale(350),
        height: heightPercentageToDP("7%"),
        backgroundColor: "white",
        borderRadius: 25,
        borderColor: COLORS.green,
        borderWidth: 2,
        alignItems: "center",
        flexDirection: "row",
        paddingHorizontal: moderateScale(20)
    },
    search: {
        fontSize: moderateScale(14),
        color: COLORS.darkGrey,
        marginLeft: moderateScale(10),
        width: moderateScale(250)
    },
    divider: {
        height: verticalScale(20),
        width: 1,
        backgroundColor: COLORS.grey,
        marginHorizontal: 10
    },
    titlteText: {
        fontSize: moderateScale(24),
        color: COLORS.white
    }
})