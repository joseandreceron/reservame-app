import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { moderateScale, verticalScale } from '../../helpers/ScailingScreen';
import { Calendar, CalendarList, Agenda } from 'react-native-calendars';


function Calendar(props) {
    return (
        <Calendar
            // onMonthChange={(month) => this.props.onMonthChange(month)}
            onDayPress={props.dateSelected ? (day) => { props.dateSelected(day) } : console.log("date Selected")}
            markedDates={props.markedDates}
            theme={{
                selectedDayBackgroundColor: '#fff',
                selectedDayTextColor: '#FFF',
                todayTextColor: '#259FC2',
            }}
        />
    )
}

export default Calendar;

const styles = StyleSheet.create({
    container: {

    },
})