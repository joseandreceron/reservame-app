import * as React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import { commonHeaderOptions, settingsScreens } from '../NavigationOptions';
import { COLORS } from '../../helpers/constants';
import Favorites from '../../views/Favorites/Favorites';

const Stack = createStackNavigator();

export function FavStack() {
  return (
    <Stack.Navigator
      initialRouteName="Favorites"
      screenOptions={{ headerShown: false }}
    >
      <Stack.Screen
        name="Favorites"
        component={Favorites}
        options={({ navigation, route }) => ({
          headerTintColor: COLORS.white,
          headerTitle: '',
          headerTitleStyle: {
            fontWeight: 'bold',
            fontSize: 22,
          },
          headerTitleAlign: 'center',
          headerStyle: {
            elevation: 0,
          },
        })}
      />

      {/* Settings Screens  */}

      {settingsScreens.map((route, i) => {
        return (
          <Stack.Screen key={i} name={route.name} component={route.screen} options={{ ...route.options }} />
        );
      })}

    </Stack.Navigator>
  );
}