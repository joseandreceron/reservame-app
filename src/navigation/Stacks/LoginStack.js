import * as React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import Login from '../../views/Login/Login';


import { commonHeaderOptions, settingsScreens } from '../NavigationOptions';
import { COLORS } from '../../helpers/constants';

const Stack = createStackNavigator();

export function LoginStack() {
    return (
        <Stack.Navigator
            initialRouteName="Login"
            screenOptions={{ ...commonHeaderOptions }}
        >

            <Stack.Screen
                name="Login"
                component={Login}
                options={{ headerShown: false }}
            />

            {/* Settings Screens  */}

            {settingsScreens.map((route, i) => {
                return (
                    <Stack.Screen
                        key={i}
                        name={route.name}
                        component={route.screen}
                        options={{ ...route.options }}
                    />
                );
            })}

        </Stack.Navigator>
    );
}