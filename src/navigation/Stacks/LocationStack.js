import * as React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import { commonHeaderOptions, settingsScreens } from '../NavigationOptions';
import { COLORS } from '../../helpers/constants';
import Location from "../../views/Locations/Locations";

const Stack = createStackNavigator();

export function LocationStack() {
  return (
    <Stack.Navigator
      initialRouteName="Location"
      screenOptions={{ headerShown: false }}
    >
      <Stack.Screen
        name="Location"
        component={Location}
        options={({ navigation, route }) => ({
          headerTintColor: COLORS.white,
          headerTitle: '',
          headerTitleStyle: {
            fontWeight: 'bold',
            fontSize: 22,
          },
          headerTitleAlign: 'center',
          headerStyle: {
            elevation: 0,
          },
        })}
      />

      {/* Settings Screens  */}

      {settingsScreens.map((route, i) => {
        return (
          <Stack.Screen key={i} name={route.name} component={route.screen} options={{ ...route.options }} />
        );
      })}

    </Stack.Navigator>
  );
}