import * as React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import { commonHeaderOptions, settingsScreens } from '../NavigationOptions';
import Home from '../../views/Home';
import { COLORS } from '../../helpers/constants';

const Stack = createStackNavigator();

export function HomeStack() {
  return (
    <Stack.Navigator
      initialRouteName="Home"
      screenOptions={{ headerShown: false }}
    >
      <Stack.Screen
        name="Home"
        component={Home}
        options={({ navigation, route }) => ({
          headerTintColor: COLORS.white,
          headerTitle: '',
          headerTitleStyle: {
            fontWeight: 'bold',
            fontSize: 22,
          },
          headerTitleAlign: 'center',
          headerStyle: {
            elevation: 0,
          },
        })}
      />

      {/* Settings Screens  */}

      {settingsScreens.map((route, i) => {
        return (
          <Stack.Screen key={i} name={route.name} component={route.screen} options={{ ...route.options }} />
        );
      })}

    </Stack.Navigator>
  );
}