// Constants

import { COLORS } from '../helpers/constants';

import Dummy from '../views/Dummy';
import SignIn from '../views/Login/SignIn';
import SignUp from '../views/Login/SignUp';
import Search from "../views/Search";
import ItemList from "../views/IntemList";
import ItemDetails from '../views/ItemDetails';
import SetDate from '../views/SetDate';

export const settingsScreens = [
    {
        name: 'Dummy',
        screen: Dummy,
        options: {
            headerTransparent: false,
            headerShown: false,
            headerTitle: 'Freight',
            headerTintColor: COLORS.black,
        }
    },
    {
        name: 'SignIn',
        screen: SignIn,
        options: {
            headerTransparent: false,
            headerShown: true,
            headerTitle: 'Iniciar Sesión',
            headerTintColor: COLORS.blackV2,
        }
    },
    {
        name: 'SignUp',
        screen: SignUp,
        options: {
            headerTransparent: false,
            headerShown: true,
            headerTitle: 'Registro',
            headerTintColor: COLORS.blackV2,
        }
    },
    {
        name: 'Search',
        screen: Search,
        options: {
            headerTransparent: false,
            headerShown: false,
            headerTintColor: COLORS.blackV2,
        }
    },
    {
        name: 'ItemList',
        screen: ItemList,
        options: {
            headerTransparent: false,
            headerShown: false,
            headerTintColor: COLORS.blackV2,
        }
    },
    {
        name: 'ItemDetails',
        screen: ItemDetails,
        options: {
            headerTransparent: false,
            headerShown: false,
            headerTintColor: COLORS.blackV2,
        }
    },
    {
        name: 'SetDate',
        screen: SetDate,
        options: {
            headerTransparent: false,
            headerShown: false,
            headerTintColor: COLORS.blackV2,
        }
    },
]


// Common Header Options

export const commonHeaderOptions = {
    headerTransparent: false,
    headerTintColor: COLORS.darkBlue,
    headerBackTitleVisible: false,
    // headerShown: false
}

export const defaulOptions = {
    headerTransparent: false,
    headerTintColor: COLORS.darkBlue,
}

export const commonWhiteHeaderOptions = {
    headertintColor: COLORS.darkBlue,
}