import * as React from 'react';

// Modules
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

// Navigators
import { HomeStack } from "./Stacks/HomeStack";
import { LocationStack } from './Stacks/LocationStack';
import { BookingsStack } from './Stacks/BookingsStack';
import { SettingsStack } from './Stacks/SettingsStack';
import { COLORS } from '../helpers/constants';
import { FavStack } from './Stacks/FavStack';
import TabNavIcon from '../components/Navigation/TabNavIcon';

// Tabs Stack ===================================================================================================================

const Tab = createBottomTabNavigator();

export function TabsStack(props) {
  return (
    <Tab.Navigator
      initialRouteName="Inicio"
      screenOptions={({ route }) => ({
        tabBarIcon: ({ focused }) => {
          let routeName = '';
          const focus = focused;

          if (route.name === 'Inicio') {
            routeName = 'home'
          } else if (route.name === 'Favoritos') {
            routeName = 'favorites'
          } else if (route.name === 'Reservas') {
            routeName = 'calendar'
          } else if (route.name === 'Ajustes') {
            routeName = 'settings'
          } else if (route.name === 'Cerca') {
            routeName = 'location'
          }

          return <TabNavIcon name={route.name} icon={routeName} focused={focused} />;
        },
      })}
      tabBarOptions={{
        activeTintColor: COLORS.orange,
        inactiveTintColor: COLORS.darkGrey,
        tabStyle: {
          backgroundColor: 'white',
        },
        labelStyle: {
          // textTransform: 'uppercase',
        },
      }}>
      <Tab.Screen name={"Favoritos"} component={FavStack} />
      <Tab.Screen name={"Reservas"} component={BookingsStack} />
      <Tab.Screen name={"Inicio"} component={HomeStack} />
      <Tab.Screen name={"Cerca"} component={LocationStack} />
      <Tab.Screen name={"Ajustes"} component={SettingsStack} />
    

    </Tab.Navigator>
  );
}
