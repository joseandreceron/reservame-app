import * as React from "react"
import Svg, { Path } from "react-native-svg"

function Location(props) {
  return (
    <Svg width={props.size} height={props.size} viewBox="0 0 15.933 22.761" {...props}>
      <Path
        data-name="Icon material-location-on"
        d="M7.966 0A7.961 7.961 0 000 7.966c0 5.975 7.966 14.795 7.966 14.795s7.966-8.82 7.966-14.795A7.961 7.961 0 007.966 0zm0 10.812a2.845 2.845 0 112.845-2.845 2.846 2.846 0 01-2.845 2.845z"
        fill={props.color ? props.color : "#474b4e"}
      />
    </Svg>
  )
}

export default Location
