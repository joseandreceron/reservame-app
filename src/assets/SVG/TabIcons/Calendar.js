import * as React from "react"
import Svg, { Path } from "react-native-svg"

function Calendar(props) {
  return (
    <Svg width={props.size} height={props.size} viewBox="0 0 17.925 20.485" {...props}>
      <Path
        data-name="Icon awesome-calendar-check"
        d="M17.444 6.4H.48A.48.48 0 010 5.921v-1.44a1.92 1.92 0 011.92-1.92h1.92V.48A.48.48 0 014.321 0h1.6A.48.48 0 016.4.48v2.081h5.121V.48A.48.48 0 0112 0h1.6a.48.48 0 01.48.48v2.081H16a1.92 1.92 0 011.92 1.92v1.44a.48.48 0 01-.476.479zM.48 7.682h16.964a.48.48 0 01.48.48v10.4A1.92 1.92 0 0116 20.485H1.92A1.92 1.92 0 010 18.565V8.162a.48.48 0 01.48-.48zm13.335 3.839l-1.127-1.136a.48.48 0 00-.679 0L7.767 14.59l-1.84-1.855a.48.48 0 00-.679 0L4.112 13.86a.48.48 0 000 .679l3.3 3.332a.48.48 0 00.679 0l5.72-5.674a.48.48 0 000-.679z"
        fill={props.color ? props.color : "#474b4e"}
      />
    </Svg>
  )
}

export default Calendar
