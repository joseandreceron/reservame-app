import * as React from "react"
import Svg, { Path } from "react-native-svg"

function Home(props) {
  return (
    <Svg width={props.size} height={props.size} viewBox="0 0 22.761 19.347" {...props}>
      <Path
        data-name="Icon material-home"
        d="M9.1 19.347v-6.828h4.552v6.828h5.69v-9.1h3.414L11.381 0 0 10.243h3.414v9.1z"
        fill={props.color ? props.color : "#474b4e"}
      />
    </Svg>
  )
}

export default Home
