import * as React from "react"
import Svg, { Path } from "react-native-svg"

function Favorites(props) {
  return (
    <Svg width={props.size} height={props.size} viewBox="0 0 22.761 20.883" {...props}>
      <Path
        data-name="Icon material-favorite"
        d="M11.381 20.883l-1.65-1.5C3.869 14.066 0 10.561 0 6.259A6.2 6.2 0 016.259 0a6.815 6.815 0 015.121 2.379A6.815 6.815 0 0116.5 0a6.2 6.2 0 016.259 6.259c0 4.3-3.869 7.807-9.73 13.133z"
        fill={props.color ? props.color : "#474b4e"}
      />
    </Svg>
  )
}

export default Favorites
