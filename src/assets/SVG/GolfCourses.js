import * as React from "react"
import Svg, { G, Path } from "react-native-svg"

function GolfCourses(props) {
  return (
    <Svg width={props.size} height={props.size} viewBox="0 0 29.818 35.08" {...props}>
      <G data-name="Icon material-golf-course" fill="#51565a">
        <Path
          data-name="Path 5"
          d="M29.818 30.695a2.631 2.631 0 11-2.631-2.631 2.631 2.631 0 012.631 2.631z"
        />
        <Path
          data-name="Path 6"
          d="M22.8 6.876L8.77 0v31.572H5.262v-3.034C2.122 29.151 0 30.274 0 31.572 0 33.5 4.718 35.08 10.524 35.08s10.524-1.58 10.524-3.508c0-1.736-3.789-3.175-8.77-3.455V12.243z"
        />
      </G>
    </Svg>
  )
}

export default GolfCourses
