import * as React from "react"
import Svg, { Path } from "react-native-svg"

function Business(props) {
  return (
    <Svg width={props.size} height={props.size} viewBox="0 0 45 36" {...props}>
      <Path
        data-name="Icon awesome-business-time"
        d="M34.875 15.75A10.125 10.125 0 1045 25.875 10.12 10.12 0 0034.875 15.75zm4.5 10.567a.685.685 0 01-.683.683h-4.259a.685.685 0 01-.683-.683v-5.384a.685.685 0 01.683-.683h.884a.685.685 0 01.683.683v3.817h2.692a.685.685 0 01.683.683zm-4.5-12.817c.38 0 .754.023 1.125.057v-3.432a3.46 3.46 0 00-3.375-3.375H27V3.375A3.46 3.46 0 0023.625 0h-11.25A3.46 3.46 0 009 3.375V6.75H3.375A3.46 3.46 0 000 10.125v5.625h27.782a12.3 12.3 0 017.093-2.25zM22.5 6.75h-9V4.5h9zm.48 15.75h-8.355a1.125 1.125 0 01-1.125-1.125V18H0v10.125A3.46 3.46 0 003.375 31.5h20.491a12.209 12.209 0 01-.887-9z"
        fill="#51565a"
      />
    </Svg>
  )
}

export default Business
