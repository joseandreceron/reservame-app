import * as React from "react"
import Svg, { Defs, ClipPath, Path, G } from "react-native-svg"

function Restaurant(props) {
  return (
    <Svg width={props.size} height={props.size} viewBox="0 0 42.875 39.673" {...props}>
      <Defs>
        <ClipPath id="prefix__a">
          <Path fill="none" d="M0 0h42.875v39.673H0z" />
        </ClipPath>
      </Defs>
      <G
        data-name="Icon ionic-md-restaurant"
        fill="#51565a"
        clipPath="url(#prefix__a)"
      >
        <Path
          data-name="Path 2"
          d="M12.021 22.309l6.388-6.225L2.606.796a8.608 8.608 0 000 12.34z"
        />
        <Path
          data-name="Path 3"
          d="M27.264 18.378c3.475 1.529 8.294.437 11.769-3.057 4.259-4.15 5.156-10.156 1.794-13.323C37.579-1.279 31.411-.4 27.152 3.745c-3.587 3.495-4.708 8.19-3.138 11.466-4.932 4.914-21.856 21.4-21.856 21.4l3.138 3.057L20.763 24.6 36.23 39.67l3.138-3.057-15.467-15.068z"
        />
      </G>
    </Svg>
  )
}

export default Restaurant
