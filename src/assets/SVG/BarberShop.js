import * as React from "react"
import Svg, { Path } from "react-native-svg"

function BarberShop(props) {
  return (
    <Svg width={props.size} height={props.size} viewBox="0 0 39.836 39.836" {...props}>
      <Path
        data-name="Icon material-content-cut"
        d="M15.217 11.234a7.762 7.762 0 00.717-3.267 7.967 7.967 0 10-7.967 7.967 7.762 7.762 0 003.267-.717l4.7 4.7-4.7 4.7a7.762 7.762 0 00-3.267-.717 7.967 7.967 0 107.967 7.967 7.762 7.762 0 00-.717-3.267l4.7-4.7 13.944 13.945h5.975v-1.992zm-7.25.717a3.984 3.984 0 113.984-3.984 3.983 3.983 0 01-3.984 3.984zm0 23.9a3.984 3.984 0 113.984-3.984 3.983 3.983 0 01-3.984 3.986zm11.951-14.937a1 1 0 111-1 .986.986 0 01-1 1zM33.861 1.992L21.91 13.943l3.984 3.984L39.836 3.984V1.992z"
        fill="#51565a"
      />
    </Svg>
  )
}

export default BarberShop
