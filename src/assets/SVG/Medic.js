import * as React from "react"
import Svg, { Path } from "react-native-svg"

function Medical(props) {
  return (
    <Svg width={props.size} height={props.size} viewBox="0 0 42.318 39.673" {...props}>
      <Path
        data-name="Icon awesome-briefcase-medical"
        d="M38.35 7.935h-6.612V3.967A3.968 3.968 0 0027.771 0H14.547a3.968 3.968 0 00-3.967 3.967v3.967H3.967A3.968 3.968 0 000 11.902v23.8a3.968 3.968 0 003.967 3.967H38.35a3.968 3.968 0 003.967-3.967v-23.8a3.968 3.968 0 00-3.967-3.967zM15.869 5.29h10.58v2.645h-10.58zm13.224 20.5a.663.663 0 01-.661.661H23.8v4.628a.663.663 0 01-.661.661h-3.964a.663.663 0 01-.661-.661V26.45h-4.629a.663.663 0 01-.661-.661V21.82a.663.663 0 01.661-.661h4.628V16.53a.663.663 0 01.661-.661h3.967a.663.663 0 01.661.661v4.628h4.628a.663.663 0 01.661.661z"
        fill="#51565a"
      />
    </Svg>
  )
}

export default Medical
