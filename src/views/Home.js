import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, Image, SafeAreaView, FlatList } from 'react-native';
import HomeCard from '../components/Cards/HomeCard';
import Header from '../components/Header/Header';
import SearchBar from '../components/SearchBar/SearchBar';
import { COLORS } from '../helpers/constants';
import { moderateScale, verticalScale } from '../helpers/ScailingScreen';

function Home(props) {
  const [state, setState] = useState('');

  useEffect(() => {
    return () => {

    }
  }, []);

  return (
    <SafeAreaView
      style={styles.container}
    >

      <Header
        title={"Reservame"}
      />

      <View style={styles.searchContainer}>
        <SearchBar
          isButton={true}
          onPress={() => props.navigation.navigate("Search")}
        />
      </View>

      <View style={styles.content}>

        <FlatList
          data={list}
          numColumns={2}
          keyExtractor={(item, index) => index.toString()}
          showsVerticalScrollIndicator={false}
          extraData={list}
          contentContainerStyle={styles.listStyles}
          renderItem={({ item }) =>
            < HomeCard
              title={item.name}
              icon={item.icon}
              onPress={() => props.navigation.navigate("ItemList", {
                data: item
              })}
            />
          }
        />

      </View>

    </SafeAreaView>
  )
}

export default Home;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.white
  },
  searchContainer: {
    justifyContent: "center",
    alignItems: "center",
    marginTop: verticalScale(-20)
  },
  content: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  listStyles: {
    marginTop: verticalScale(30),
  }
})


const list = [
  {
    "name": "Restaurante",
    "icon": "restaurant"
  },
  {
    "name": "Citas con Médicos",
    "icon": "medic"
  },
  {
    "name": "Barberias",
    "icon": "barbershop"
  },
  {
    "name": "Salon de Belleza",
    "icon": "haircare"
  },
  {
    "name": "Spa",
    "icon": "spa"
  },
  {
    "name": "Campos de Golf",
    "icon": "golfcourse"
  },
  {
    "name": "Citas Empresariales",
    "icon": "business"
  },
  {
    "name": "Dentistas",
    "icon": "dentist"
  },

]