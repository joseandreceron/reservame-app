import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';

class Bookings extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <View style={styles.container}>
        <Text> Bookings </Text>
      </View>
    );
  }
}

export default Bookings;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: "center"
  }
})