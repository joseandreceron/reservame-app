import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, Image, SafeAreaView, FlatList, StatusBar, TextInput, TouchableOpacity } from 'react-native';
import HeaderImageScrollView, { TriggeringView } from 'react-native-image-header-scroll-view';

import FixedForeground from "../components/Header/FixedForeground";
import DatePicker from '../components/Pickers/DatePicker';
import Button from "../components/Buttons/Button";

import { COLORS } from '../helpers/constants';
import { moderateScale, verticalScale, MIN_HEIGHT, MAX_HEIGHT, scale } from '../helpers/ScailingScreen';

import Ionicons from "react-native-vector-icons/Ionicons";
import Moment from 'moment';

var dateNow = Date.now();
var currentDate = new Date();
var currentTime = currentDate.getTime();

function ItemDetails(props) {
    const [isLoading, setIsLoading] = useState(true);
    const [data, setData] = useState({});
    const [date, setDate] = useState(Moment(dateNow).format('DD-MM-YYYY'));
    const [time, settime] = useState(currentTime)

    useEffect(() => {
        const getProps = () => {
            const data = props.route.params.data;
            setData(data);
            setIsLoading(false)
        }
        getProps();
    }, []);

    const getTime = (value) => {
        console.log("Time", value)
    };

    const getDate = (value) => {
        console.log("Date", value)
    };

    return (
        <View style={styles.container}>
            <StatusBar barStyle="light-content" />

            <HeaderImageScrollView
                maxHeight={moderateScale(220)}
                minHeight={verticalScale(100)}
                maxOverlayOpacity={0.3}
                minOverlayOpacity={0.2}
                fadeOutForeground
                headerImage={isLoading ? require("../assets/images/no_image.png") : { uri: `${data.image}` }}
                renderFixedForeground={() => <FixedForeground title={data.name} subtitle={data.address} navigation={props.navigation} />}
            >
                <TriggeringView style={styles.section}>

                    <Text style={styles.title}>Detalles de Reserva</Text>

                    <View style={styles.content}>
                        <Text style={styles.subtitle}>Fecha & hora</Text>
                        <View style={styles.wrapper}>
                            <View style={styles.dateContainer}>
                                <Ionicons
                                    name={"calendar"}
                                    size={30}
                                    color={COLORS.darkGrey}
                                    style={styles.dateIcon}
                                />
                                <DatePicker
                                    value={date}
                                    action={(e) => getDate(e)}
                                    placeholder={"Date"}
                                    mode={'date'}
                                />
                            </View>

                            <View style={styles.dateContainer}>
                                <Ionicons
                                    name={"time"}
                                    size={30}
                                    color={COLORS.darkGrey}
                                    style={styles.dateIcon}
                                />
                                <DatePicker
                                    value={time}
                                    action={(e) => getTime(e)}
                                    placeholder={"Time"}
                                    mode={'time'}
                                />
                            </View>
                        </View>

                        <Text style={styles.subtitle}>Cantidad de personas</Text>

                        <View style={styles.textInputContainer}>
                            <Ionicons
                                name={"people"}
                                size={30}
                                color={COLORS.darkGrey}
                                style={styles.dateIcon}
                            />

                            <TextInput
                                style={styles.peopleInput}
                                placeholderTextColor={COLORS.darkGrey}
                                keyboardType='number-pad'
                                maxLength={2}
                            />
                        </View>
                    </View>

                </TriggeringView>
            </HeaderImageScrollView>

            <View style={styles.buttonContainer}>
                <Button
                    title={"Reservar"}
                    titleColor={COLORS.white}
                    backgroundColor={COLORS.green}
                    // onPress={() => props.navigation.navigate("SetDate")}
                />
            </View>

        </View>
    )
}

export default ItemDetails;


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLORS.white
    },
    section: {
        padding: scale(15),
    },
    title: {
        fontSize: moderateScale(26),
        color: COLORS.darkGrey
    },
    content: {
        marginTop: verticalScale(20),
    },
    subtitle: {
        fontSize: moderateScale(20),
        marginBottom: verticalScale(15),
        color: COLORS.darkGrey
    },
    wrapper: {
        flexDirection: "row",
        justifyContent: "space-between",
    },
    dateContainer: {
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        borderBottomWidth: 1,
        borderColor: COLORS.grey,
        marginBottom: verticalScale(15)
    },
    dateIcon: {
        marginRight: moderateScale(10)
    },
    textInputContainer: {
        flexDirection: "row",
        width: "50%",
        height: verticalScale(45),
        alignItems: "center",
        borderBottomWidth: 1,
        borderColor: COLORS.grey,
    },
    peopleInput: {
        width: "50%",
        height: verticalScale(40),
        fontSize: moderateScale(14),
        color: COLORS.darkGrey
    },
    buttonContainer: {
        justifyContent: "center",
        alignItems: "center",
        position: "absolute",
        bottom: verticalScale(20),
        alignSelf: "center"
    }
})

