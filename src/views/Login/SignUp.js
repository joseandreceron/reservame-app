import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, Image, SafeAreaView } from 'react-native';
import { COLORS } from '../../helpers/constants';
import { scale, verticalScale } from '../../helpers/ScailingScreen';

import Input from "../../components/Inputs/Inputs";
import Button from '../../components/Buttons/Button';

function SignUp(props) {
    const [state, setState] = useState('');
    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.inputContent}>
                <Input
                    placeholder={"Nombre"}
                />

                <Input
                    placeholder={"Apellido"}
                />

                <Input
                    placeholder={"Correo"}
                />

                <Input
                    placeholder={"Contraseña"}
                />

                <Input
                    placeholder={"Confirme su contraseña"}
                />

            </View>

            <View style={styles.buttonContainer}>
                <Button
                    backgroundColor={COLORS.green}
                    title={"Continuar"}
                    titleColor={COLORS.white}
                    onPress={() => navigation.navigate("SignIn")}
                />

                <Button
                    backgroundColor={COLORS.facebook}
                    title={"Entrar con Facebook"}
                    titleColor={COLORS.white}
                />

                <Button
                    backgroundColor={COLORS.white}
                    title={"Entrar con Google"}
                    titleColor={COLORS.black}
                />
            </View>
        </SafeAreaView>
    )
}

export default SignUp;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLORS.white,
    },
    inputContent: {
        padding: scale(15),
        marginTop: verticalScale(20)
    },
    buttonContainer: {
        alignItems: "center",
        position: "absolute",
        bottom: verticalScale(60),
        alignSelf: "center"
    }
})