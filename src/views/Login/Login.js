import React, { useState, useEffect, useContext } from 'react';
import { View, Text, StyleSheet, Image, SafeAreaView } from 'react-native';
import Button from '../../components/Buttons/Button';
import { COLORS } from '../../helpers/constants';
import { moderateScale } from '../../helpers/ScailingScreen';

export const Login = (({ navigation }) => {
    const [state, setState] = useState('');
    useEffect(() => {
        return () => {
        }
    }, []);

    return (
        <SafeAreaView style={styles.container}>

            <Text style={styles.title}>Bienvenido</Text>

            <Image
                source={require("../../assets/Logo/logo_clear.png")}
                style={styles.logo}
            />

            <View style={styles.buttonsContainer}>

                <Button
                    backgroundColor={COLORS.orange}
                    title={"Iniciar Sesión"}
                    titleColor={COLORS.white}
                    onPress={() => navigation.navigate("SignIn")}
                />

                <Button
                    backgroundColor={COLORS.green}
                    title={"Registro"}
                    titleColor={COLORS.white}
                    onPress={() => navigation.navigate("SignUp")}
                />

                <Button
                    backgroundColor={COLORS.facebook}
                    title={"Entrar con Facebook"}
                    titleColor={COLORS.white}
                />

                <Button
                    backgroundColor={COLORS.white}
                    title={"Entrar con Google"}
                    titleColor={COLORS.black}
                />
            </View>

        </SafeAreaView>
    );
}
)
export default Login;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center",
        justifyContent: 'space-evenly',
        backgroundColor: COLORS.white
    },
    title: {
        fontSize: moderateScale(40),
        fontWeight: "bold"
    },
    logo: {
        height: 300,
        width: 300
    },
    buttonsContainer: {

    }
})