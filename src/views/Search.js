import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, Image, SafeAreaView, FlatList, StatusBar } from 'react-native';

import RestaurantCards from '../components/Cards/RestaurantCards';
import SearchBar from '../components/SearchBar/SearchBar';

import { COLORS } from '../helpers/constants';
import { moderateScale, verticalScale } from '../helpers/ScailingScreen';

function Search(props) {
    const [state, setState] = useState('');

    useEffect(() => {
        return () => {

        }
    }, []);

    return (
        <SafeAreaView
            style={styles.container}
        >
            <StatusBar barStyle="light-content" />

            <View style={styles.searchContainer}>
                <View style={styles.searchContent}>
                    <SearchBar
                        isButton={false}
                        isBack={true}
                        onBackPress={() => props.navigation.goBack()}
                    />
                </View>
            </View>

            <View style={styles.content}>
                <FlatList
                    data={list}
                    keyExtractor={(item, index) => index.toString()}
                    showsVerticalScrollIndicator={false}
                    extraData={list}
                    contentContainerStyle={styles.listStyles}
                    renderItem={({ item }) =>
                        <RestaurantCards
                            title={item.name}
                            schedule={item.schedule}
                            address={item.address}
                            image={item.image}
                        />
                    }
                />

            </View>

        </SafeAreaView>
    )
}

export default Search;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLORS.white
    },
    searchContainer: {
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: COLORS.green,
        height: "20%",
        marginTop: verticalScale(-50),
    },
    searchContent: {
        marginTop: verticalScale(40)
    },
    content: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center"
    },
    listStyles: {
        // marginTop: verticalScale(30)
    }
})


const list = [
    {
        "name": "Restaurante Juan",
        "schedule": "7:00 AM - 9:00 PM",
        "address": "C/ Ejemplo #1, SDE",
        "image": "https://www.casadecampo.com.do/wp-content/uploads/2019/01/dining-minitas-beach-club-restaurant.jpg"
    },
    {
        "name": "Pedro Restaurant",
        "schedule": "7:00 AM - 9:00 PM",
        "address": "C/ Ejemplo #1, SDE",
        "image": "https://media-cdn.tripadvisor.com/media/photo-s/0a/c5/3c/7a/shibuya-ichiban.jpg"
    },
    {
        "name": "Jose Bar",
        "schedule": "7:00 AM - 9:00 PM",
        "address": "C/ Ejemplo #1, SDE",
        "image": "https://www.lessings.com/webphotos/20161031_121335_000.jpg"
    },
]