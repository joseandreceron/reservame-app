import { moderateScale, verticalScale } from "./ScailingScreen";

// Colors

export const COLORS = {
  orange: "#FFA830",
  green: "#009060",
  lightGreen: "#48C090",
  facebook: "#0077EE",
  grey: '#F2F2F2',
  darkGrey: "#707070",
  white: "#FFFF",
  black: "black",
  lightBlack: 'rgba(0, 0,0, 0.6)',
  blackV2: "#3B3B3B",
  red: "red"
};

// Sizes 
export const SIZES = {
  contentTextSize: moderateScale(13),
  borderRadius: 5
}

