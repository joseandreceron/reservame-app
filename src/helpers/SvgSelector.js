import * as React from "react"

import Medic from '../assets/SVG/Medic';
import Business from '../assets/SVG/Business';
import Dentist from '../assets/SVG/Dentist';
import HairCare from '../assets/SVG/HairCare';
import Spa from '../assets/SVG/Spa';
import BarberShop from '../assets/SVG/BarberShop';
import GolfCourses from '../assets/SVG/GolfCourses';
import Restaurant from '../assets/SVG/Restaurant';

//Tab Icons
import Calendar from "../assets/SVG/TabIcons/Calendar";
import Favorites from "../assets/SVG/TabIcons/Favorites";
import Home from "../assets/SVG/TabIcons/Home";
import Location from "../assets/SVG/TabIcons/Location";
import Settings from "../assets/SVG/TabIcons/Settings";


export function SvgSelector(props) {
    let icon = null
    switch (props.icons) {
        case 'medic':
            icon = (<Medic size={props.size ? props.size : 25} />)
            break;
        case 'business':
            icon = (<Business size={props.size ? props.size : 25} />)
            break;
        case 'dentist':
            icon = (<Dentist size={props.size ? props.size : 25} />)
            break;
        case 'haircare':
            icon = (<HairCare size={props.size ? props.size : 25} />)
            break;
        case 'spa':
            icon = (<Spa size={props.size ? props.size : 25} />)
            break;
        case 'barbershop':
            icon = (<BarberShop size={props.size ? props.size : 25} />)
            break;
        case 'golfcourse':
            icon = (<GolfCourses size={props.size ? props.size : 25} />)
            break;
        case 'restaurant':
            icon = (<Restaurant size={props.size ? props.size : 25} />)
            break;
        //Tab Icons
        case 'calendar':
            icon = (<Calendar size={props.size ? props.size : 25} color={props.color ? props.color : "#474b4e"} />)
            break;
        case 'favorites':
            icon = (<Favorites size={props.size ? props.size : 25} color={props.color ? props.color : "#474b4e"} />)
            break;
        case 'home':
            icon = (<Home size={props.size ? props.size : 25} color={props.color ? props.color : "#474b4e"} />)
            break;
        case 'location':
            icon = (<Location size={props.size ? props.size : 25} color={props.color ? props.color : "#474b4e"} />)
            break;
        case 'settings':
            icon = (<Settings size={props.size ? props.size : 25} color={props.color ? props.color : "#474b4e"} />)
            break;

    }
    return icon
}