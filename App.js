import React, { Component } from 'react';
import { View, Text } from 'react-native';

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import Login from './src/views/Login/Login';
import { HomeStack } from './src/navigation/Stacks/HomeStack';
import { LoginStack } from './src/navigation/Stacks/LoginStack';
import { TabsStack } from './src/navigation/TabStack';

const Stack = createStackNavigator();

class App extends Component {
  render() {
    return (
      <NavigationContainer>
        <Stack.Navigator>

          <Stack.Screen
            name="Login"
            component={LoginStack}
            options={{ headerShown: false }}
          />

          <Stack.Screen
            name="Home"
            component={TabsStack}
            options={{ headerShown: false }}
          />

        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}

export default App;
